<?php

namespace ApiBundle\Entity;

/**
 * TCalificaciones
 *
 */
class TCalificaciones
{
    /**
     * @var integer
     */
    private $idTCalificaciones;

    /**
     * @var float
     *
     */
    private $calificacion;

    /**
     * @var \DateTime
     *
     */
    private $fechaRegistro;

    /**
     * @var \ApiBundle\Entity\TMaterias
     *
     */
    private $idTMaterias;

    /**
     * Get idTCalificaciones
     *
     * @return integer
     */
    public function getIdTCalificaciones()
    {
        return $this->idTCalificaciones;
    }

    /**
     * Set calificacion
     *
     * @param float $calificacion
     *
     * @return TCalificaciones
     */
    public function setCalificacion($calificacion)
    {
        $this->calificacion = $calificacion;

        return $this;
    }

    /**
     * Get calificacion
     *
     * @return float
     */
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * Set fechaRegistro
     *
     * @param \DateTime $fechaRegistro
     *
     * @return TCalificaciones
     */
    public function setFechaRegistro($fechaRegistro)
    {
        $this->fechaRegistro = $fechaRegistro;

        return $this;
    }

    /**
     * Get fechaRegistro
     *
     * @return \DateTime
     */
    public function getFechaRegistro()
    {
        return $this->fechaRegistro;
    }

    /**
     * Set idTMaterias
     *
     * @param \ApiBundle\Entity\TMaterias $idTMaterias
     *
     * @return TCalificaciones
     */
    public function setIdTMaterias(\ApiBundle\Entity\TMaterias $idTMaterias = null)
    {
        $this->idTMaterias = $idTMaterias;

        return $this;
    }

    /**
     * Get idTMaterias
     *
     * @return \ApiBundle\Entity\TMaterias
     */
    public function getIdTMaterias()
    {
        return $this->idTMaterias;
    }

    /**
     * @var \ApiBundle\Entity\TAlumnos
     */
    private $idTUsuarios;


    /**
     * Set idTUsuarios
     *
     * @param \ApiBundle\Entity\TAlumnos $idTUsuarios
     *
     * @return TCalificaciones
     */
    public function setIdTUsuarios(\ApiBundle\Entity\TAlumnos $idTUsuarios = null)
    {
        $this->idTUsuarios = $idTUsuarios;

        return $this;
    }

    /**
     * Get idTUsuarios
     *
     * @return \ApiBundle\Entity\TAlumnos
     */
    public function getIdTUsuarios()
    {
        return $this->idTUsuarios;
    }
}

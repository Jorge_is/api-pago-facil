<?php

namespace ApiBundle\Entity;

/**
 * TMaterias
 *
 */
class TMaterias
{
    /**
     * @var integer
     *
     */
    private $idTMaterias;

    /**
     * @var string
     *
     */
    private $nombre;

    /**
     * @var integer
     *
     */
    private $activo;

    /**
     * Get idTMaterias
     *
     * @return integer
     */
    public function getIdTMaterias()
    {
        return $this->idTMaterias;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return TMaterias
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set activo
     *
     * @param integer $activo
     *
     * @return TMaterias
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return integer
     */
    public function getActivo()
    {
        return $this->activo;
    }
}

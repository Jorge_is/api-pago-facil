<?php

namespace ApiBundle\Entity;

/**
 * TAlumnos
 *
 */
class TAlumnos
{
    /**
     * @var integer
     *
     */
    private $idTUsuarios;

    /**
     * @var string
     *
     */
    private $nombre;

    /**
     * @var string
     *
     */
    private $apPaterno;

    /**
     * @var string
     *
     */
    private $apMaterno;

    /**
     * @var integer
     *
     */
    private $activo;

    /**
     * Get idTUsuarios
     *
     * @return integer
     */
    public function getIdTUsuarios()
    {
        return $this->idTUsuarios;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return TAlumnos
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apPaterno
     *
     * @param string $apPaterno
     *
     * @return TAlumnos
     */
    public function setApPaterno($apPaterno)
    {
        $this->apPaterno = $apPaterno;

        return $this;
    }

    /**
     * Get apPaterno
     *
     * @return string
     */
    public function getApPaterno()
    {
        return $this->apPaterno;
    }

    /**
     * Set apMaterno
     *
     * @param string $apMaterno
     *
     * @return TAlumnos
     */
    public function setApMaterno($apMaterno)
    {
        $this->apMaterno = $apMaterno;

        return $this;
    }

    /**
     * Get apMaterno
     *
     * @return string
     */
    public function getApMaterno()
    {
        return $this->apMaterno;
    }

    /**
     * Set activo
     *
     * @param integer $activo
     *
     * @return TAlumnos
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return integer
     */
    public function getActivo()
    {
        return $this->activo;
    }
}

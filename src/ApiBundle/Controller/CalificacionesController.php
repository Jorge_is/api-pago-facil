<?php
/**
 * Created by PhpStorm.
 * User: Jorge
 * Date: 26/08/18
 * Time: 16:55
 */

namespace ApiBundle\Controller;


use ApiBundle\Entity\User;
use ApiBundle\Entity\TCalificaciones; 
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\HttpFoundation\JsonResponse;

class CalificacionesController extends Controller
{
    /**
     * Descripcion especifica de este metodo
     *
     * @ApiDoc(
     *  section = "Calificaciones",
     *  description="Alta de calificacion por alumno",
     *  requirements={
     *     {"name"="materia",   "dataType"="integer", "required"=true, "description"="Materia ID"},
     *     {"name"="alumno",    "dataType"="integer", "required"=true, "description"="Alumno ID"},
     *     {"name"="calificacion",   "dataType"="integer", "required"=false, "description"="Calificacion"}
     *   },
     * headers={
     *     {"name"="Authorization", "dataType"="string", "required"=true, "description"="token authorization"}
     *   }
     * )
     */
    public function altaCalificacionAction(Request $request)
    {
        $helpers = $this->get('app.helpers');
        $validator = $this->get('validator');

        $materia = $request->get('materia');
        $alumno = $request->get('alumno');
        $calificacion = $request->get('calificacion');
        $em = $this->getDoctrine()->getManager();

        $num_decimal = (float)$calificacion;

        $calificacion = new TCalificaciones();

        if($materia && $alumno && $calificacion){
            $calificacion->setCalificacion($num_decimal);
            $materia_db = $em->getRepository('ApiBundle:TMaterias')->find($materia);
            $calificacion->setIdTMaterias($materia_db);
            $alumno_db = $em->getRepository('ApiBundle:TAlumnos')->find($alumno);
            if($alumno_db){
                $calificacion->setIdTUsuarios($alumno_db);
            }else{
                $data = array(
                    'error' => 'not found',
                    'msg' => 'No se encontro alumno con ese id'
                );
    
                $response = $helpers->responseHeaders(404, $data);
    
                return $response;
            }
            
            $calificacion->setFechaRegistro(new \DateTime());
    
            $errors = $validator->validate($calificacion);
                foreach ($errors as $error) {
                    $messages[] = $error->getMessage();
                }
    
                if (count($errors) > 0) {
                    $response = $helpers->responseHeaders(400, $messages);
                } else {
                    $em->persist($calificacion);
                    $flush = $em->flush();
    
                    if ($flush == null) {
                        $data = array(
                            'success' => 'ok',
                            'msg' => 'calificacion registrada'
                        );
    
                        $response = $helpers->responseHeaders(200, $data);
                    }
                }
        }else{
            $data = array(
                'error' => 'bad request',
                'msg' => 'los campos materia, alumno y calificacion son obligatorios'
            );

            $response = $helpers->responseHeaders(400, $data);
        }
        
        return $response;    
    }

    /**
     * Descripcion especifica de este metodo
     *
     * @ApiDoc(
     *  section = "Calificaciones",
     *  description="Listado de Calificaciones por alumno",
     *  requirements={
     *     {"name"="id",   "dataType"="integer", "required"=true, "description"="Alumno ID"}
     *   },
     * headers={
     *     {"name"="Authorization", "dataType"="string", "required"=true, "description"="token authorization"}
     *   }
     * )
     */
    public function listadoCalificacionesAction(Request $request, $id = null)
    {
        $helpers = $this->get('app.helpers');

        $em = $this->getDoctrine()->getManager();

        if($id != null && $id != '{id}'){
            $calificaciones = $em->getRepository('ApiBundle:TCalificaciones')->findBy(array('idTUsuarios' => $id));

            if($calificaciones){
                $calificaciones_db = [];
                $numCalif = [];
        
                foreach ($calificaciones as $key => $value) {
        
                    array_push($calificaciones_db, array(
                        'id' => $value->getIdTUsuarios()->getIdTUsuarios(),
                        'nombre' => $value->getIdTUsuarios()->getNombre(),
                        'apellidos' => $value->getIdTUsuarios()->getApPaterno() . " " . $value->getIdTUsuarios()->getApMaterno(),
                        'materia' => $value->getIdTMaterias()->getNombre(),
                        'calificacion' => $value->getCalificacion(),
                        'fecha_registro' => $value->getFechaRegistro()->format('d/m/Y')
                    ));
        
                   array_push($numCalif, $value->getCalificacion());      
                }
                
                $promedio = array_sum($numCalif) / (count($calificaciones));
                
                // falta redondear el promedio a 2 digitos
                $calificaciones_db['promedio'] = number_format($promedio, 1, '.', '');
        
                $data = array(
                    'success' => 'ok',
                    'data' => array($calificaciones_db)
                );
        
                $response = $helpers->responseHeaders(200, $data);
            }else{
                $data = array(
                    'error' => 'not found',
                    'msg' => 'No se encontraron calificaciones con el ID alumno solicitado'
                );
        
                $response = $helpers->responseHeaders(404, $data);
            }            

        }else{
            $data = array(
                'error' => 'bad request',
                'msg' => 'El parametro ID es obligatorio'
            );
    
            $response = $helpers->responseHeaders(404, $data);
        }
        

        return $response;
    
    }

    /**
     * Descripcion especifica de este metodo
     *
     * @ApiDoc(
     *  section = "Calificaciones",
     *  description="Actualiza una calificación",
     *  requirements={
     *     {"name"="id",   "dataType"="integer", "required"=true, "description"="Calificacion ID"},
     *     {"name"="calificacion",   "dataType"="integer", "required"=true, "description"="Calificacion"}
     *   },
     * headers={
     *     {"name"="Authorization", "dataType"="string", "required"=true, "description"="token authorization"}
     *   }
     * )
     */
    public function updateCalificacionAction(Request $request, $id = null)
    {
        $helpers = $this->get('app.helpers');
        $calif = $request->get('calificacion');
    
        $em = $this->getDoctrine()->getManager();

        if( $id != null && $id != '{id}'){
            if($calif != null && $calif != '{calif}'){
                $calificacion = $em->getRepository('ApiBundle:TCalificaciones')->findOneBy(array('idTCalificaciones' => $id));
                if($calificacion){
                    $calif = (float)$calif;
                    $calificacion->setCalificacion($calif);
                    $em->persist($calificacion);
                    $flush = $em->flush();
        
                    if ($flush == null) {
                        $data = array(
                            'success' => 'ok',
                            'msg' => 'calificacion actualizada'
                        );
                        $response = $helpers->responseHeaders(200, $data);
                    }            
        
                }else{
                    $data = array(
                        'error' => 'not found',
                        'msg' => 'calificacion no encontrada en la DB'
                    );
                    $response = $helpers->responseHeaders(404, $data);
                }
            }else{
                $data = array(
                    'error' => 'bad request',
                    'msg' => 'el campo calificacion calificacion no debe ir vacio'
                );
                $response = $helpers->responseHeaders(400, $data);
            }
            
        }else{
            $data = array(
                'error' => 'bad request',
                'msg' => 'el campo calificacion Id no debe ir vacio'
            );
            $response = $helpers->responseHeaders(400, $data);
        }

        return $response;

    }

    /**
     * Descripcion especifica de este metodo
     *
     * @ApiDoc(
     *  section = "Calificaciones",
     *  description="Elimina una calificación",
     *  requirements={
     *     {"name"="id",   "dataType"="integer", "required"=true, "description"="Calificacion ID"},
     *   },
     * headers={
     *     {"name"="Authorization", "dataType"="string", "required"=true, "description"="token authorization"}
     *   }
     * )
     */
    public function deleteCalificacionAction(Request $request, $id = null)
    {
        $helpers = $this->get('app.helpers');
        $em = $this->getDoctrine()->getManager();

        if($id != null && $id != '{id}'){
            $calificacion = $em->getRepository('ApiBundle:TCalificaciones')->findOneBy(array('idTCalificaciones' => $id));

            if($calificacion){
                $em->remove( $calificacion );
                $flush = $em->flush();
    
                if ($flush == null) {
                    $data = array(
                        'success' => 'ok',
                        'msg' => 'calificacion eliminada'
                    );
                    $response = $helpers->responseHeaders(200, $data);
                }
            }else{
                $data = array(
                    'error' => 'not found',
                    'msg' => 'calificacion no encontrada en la BD'
                );
                $response = $helpers->responseHeaders(404, $data);
            }
            
        }else{
            $data = array(
                'error' => 'bad request',
                'msg' => 'el campo id es obligatorio'
            );
            $response = $helpers->responseHeaders(400, $data);
        }

        return $response;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Jorge
 * Date: 19/08/18
 * Time: 16:55
 */

namespace ApiBundle\Controller;


use ApiBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends Controller
{
    /**
     * Descripcion especifica de este metodo
     *
     * @ApiDoc(
     *  section = "Login",
     *  description="Login de Usuarios",
     *  requirements={
     *     {"name"="email",     "dataType"="string", "required"=true, "description"="Email"},
     *     {"name"="password",  "dataType"="string", "required"=true, "description"="Password"},
     *     {"name"="gethash",   "dataType"="boolean", "required"=false, "description"="gethash"}
     *   }
     * )
     */
    public function loginAction(Request $request)
    {
        $helpers = $this->get('app.helpers');
        $jwt_auth = $this->get('app.jwt_auth');

        $email = $request->get('email');
        $password = $request->get('password');
        $hash = $request->get('gethash');

        $pwd = hash('sha256', $password);

        if ($email && $password) {

            if($hash == null){
                $signp = $jwt_auth->signup($email, $pwd);
            }else{
                $signp = $jwt_auth->signup($email, $pwd, true);
            }

            return new JsonResponse($signp);

        } else {
            $data = array(
                'code' => 400,
                'status' => 'Bad Request',
                'msj' => 'Params missing'
            );

            $response = $helpers->responseHeaders(400, $data);
        }

        return $response;

    }

    /**
     * Descripcion especifica de este metodo
     *
     * @ApiDoc(
     *  section = "User",
     *  description="Listado de Usarios",
     *  requirements={
     *   }
     * )
     */
    public function getUsersAction()
    {
        $helpers = $this->get('app.helpers');
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('ApiBundle:User')->findAll();

        $data = array(
            'status' => 'success',
            'data' => $users
        );

        return $helpers->json($data);
    }

    /**
     * Descripcion especifica de este metodo
     *
     * @ApiDoc(
     *  section = "User",
     *  description="Creacion de Usuarios",
     *  requirements={
     *     {"name"="name",      "dataType"="string", "required"=true, "description"="Name"},
     *     {"name"="lastName",  "dataType"="string", "required"=true, "description"="Last Name"},
     *     {"name"="email",     "dataType"="string", "required"=true, "description"="Email"},
     *     {"name"="password",  "dataType"="string", "required"=true, "description"="Password"},
     *     {"name"="username",  "dataType"="string", "required"=true, "description"="Username"}
     *   },
     *  headers={
     *     {"name"="Authorization", "dataType"="string", "required"=true, "description"="token authorization"}
     *    }
     * )
     */
    public function addUserAction(Request $request)
    {
        $helpers = $this->get('app.helpers');
        $validator = $this->get('validator');

        $name = $request->get('name');
        $lastName = $request->get('lastName');
        $email = $request->get('email');
        $password = $request->get('password');
        $useraname = $request->get('username');

        $em = $this->getDoctrine()->getManager();

        $user_exist = $em->getRepository('ApiBundle:User')->findOneBy(['email' => $email]);

        if (count($user_exist) == 0) {
            $user = new User();

            $user->setName($name);
            $user->setLastName($lastName);
            $user->setEmail($email);
            $user->setUsername($useraname);
            $user->setCreatedAt(new \DateTime());

            // cifrar password
            $pwd = hash('sha256', $password);
            $user->setPassword($pwd);

            $errors = $validator->validate($user);
            foreach ($errors as $error) {
                $messages[] = $error->getMessage();
            }

            if (count($errors) > 0) {
                $response = $helpers->responseHeaders(400, $messages);
            } else {
                $em->persist($user);
                $flush = $em->flush();

                if ($flush == null) {
                    $data = array(
                        'code' => 200,
                        'status' => 'success',
                        'msj' => 'User created'
                    );

                    $response = $helpers->responseHeaders(200, $data);
                }
            }
        } else {
            $data = array(
                'code' => 400,
                'status' => 'bad request',
                'msj' => 'User email exist en DB'
            );

            $response = $helpers->responseHeaders(400, $data);
        }

        return $response;

    }

    /**
     * Descripcion especifica de este metodo
     *
     * @ApiDoc(
     *  section = "User",
     *  description="Update de Usuarios",
     *  requirements={
     *     {"name"="id",        "dataType"="string", "required"=true, "description"="id user"},
     *     {"name"="name",      "dataType"="string", "required"=true, "description"="Name"},
     *     {"name"="lastName",  "dataType"="string", "required"=true, "description"="Last Name"},
     *     {"name"="email",     "dataType"="string", "required"=true, "description"="Email"}
     *   }
     * )
     */
    /*public function editUserAction(Request $request, $id = null)
    {
        $helpers = $this->get('app.helpers');

        $name = $request->get('name');
        $lastName = $request->get('lastName');
        $email = $request->get('email');

        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('ApiBundle:User')->find($id);

        if ($user != null) {

            if ($name != null) {
                $user->setName($name);
            }
            if ($lastName != null) {
                $user->setLastName($lastName);
            }
            if ($email != null) {
                $user->setEmail($email);
            }

            $em->persist($user);
            $em->flush();

            $data = array(
                'code' => 200,
                'status' => 'Success',
                'msj' => 'User update'
            );

            $response = $helpers->responseHeaders(200, $data);

        } else {
            $data = array(
                'code' => 404,
                'status' => 'Not found',
                'msj' => 'User id dont exist en DB'
            );

            $response = $helpers->responseHeaders(404, $data);
        }

        return $response;
    }*/


}
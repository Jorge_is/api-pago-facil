<?php
/**
 * Created by PhpStorm.
 * User: Jorge
 * Date: 19/08/18
 * Time: 21:26
 */

namespace ApiBundle\Services;

use Firebase\JWT\JWT;


class JwtAuth
{
    public $manager;
    public $key;

    public function __construct($manager)
    {
        $this->manager = $manager;
        $this->key = "clave secreta";
    }


    public function signup($email, $password, $getHash = null)
    {
        $key = $this->key;
        $user = $this->manager->getRepository("ApiBundle:User")->findOneBy(['email' => $email, 'password' => $password]);

        $signup = false;
        if(is_object($user)){
            $signup = true;
        }

        if($signup == true){

            $token = array(
                'id' => $user->getId(),
                'name' => $user->getName(),
                'lastName' => $user->getLastName(),
                'username' => $user->getUsername(),
                'email' => $user->getEmail(),
                'iat' => time(),
                'exp' => time() + (7 * 24 * 60 * 60)
            );

            $jwt = JWT::encode($token, $key, 'HS256');
            $decoded = JWT::decode($jwt, $key, array('HS256'));

            if($getHash != null){
                return $jwt;
            }else{
                var_dump($decoded);
                $decoded->jwt = $jwt;
                return $decoded;
            }

        }else{
            return array(
                'status' => 'error',
                'data' => 'Login failded'
            );
        }
    }

    public function checkToken($jwt, $getIdentity = false){

        $key = $this->key;
        $auth = false;

        try{
            $decoded = JWT::decode($jwt, $key, array('HS256'));
        }catch (\UnexpectedValueException $e){
            $auth = false;
        }catch (\DomainException $e){
            $auth = false;
        }

        if(isset($decoded->id)){
            $auth = true;
        }else{
            $auth = false;
        }

        if($getIdentity == true  && $auth > 0){
            return $decoded;
        }else{
            return $auth;
        }

    }
}
<?php

/**
 * Created by PhpStorm.
 * User: Jorge
 * Date: 19/08/18
 * Time: 17:24
 */

namespace ApiBundle\Services;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;


class Helpers
{
    public $jwt_auth;

    public function __construct($jwt_auth)
    {
        $this->jwt_auth = $jwt_auth;
    }

    public function authCheck($hash, $getIdentity = false){
        $jwt_auth = $this->jwt_auth;

        $auth = false;

        if($hash != null){
            if($getIdentity == false){
                $check_token = $jwt_auth->checkToken($hash);
                if($check_token == true){
                    $auth = true;
                }
            }else{
                $check_token = $jwt_auth->checkToken($hash, true);
                if(is_object($check_token)){
                    $auth = $check_token;
                }
            }
        }

        return $auth;
    }

    public function json($data)
    {
        $normalizer = array(new GetSetMethodNormalizer());
        $encoders = array("json" => new JsonEncoder());
        $serializer = new Serializer($normalizer, $encoders);

        $json = $serializer->serialize($data, 'json');

        $response = new Response(); //respuesta http

        $response->setContent($json);

        $response->headers->set("Content-Type", "application/json");

        return $response;

    }

    public function responseHeaders($code = null, $data = null)
    {
        $response = new Response();

        $response->setStatusCode($code);
        $response->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/json');
        $response->send();

        return new Response();
    }

}
api-pago-facil
==============

A Symfony project created on September 26, 2018, 10:42 pm.

_INSTALACION DEL PROYECTO:_

- Crear la BD “escuela” vacía(de momento), en su manejador de BD preferido.

- Entrar a la carpeta del proyecto y correr el siguiente comando en la consola:
	**composer install**

- Enseguida dentro de la misma consola, se te pregunta sobre los parámetros para la conexión a la BD(favor de escrinirlos).
	*Nota: los campos “mailer_tranport”, “mailer_host”, “mailer_user”, “mailer_password” y “secret” darles solo la tecla “enter”

- Después correr el siguiente comando en la consola dentro de la carpeta del proyecto: 
	**php bin/console doctrine:schema:update --force**

- Después correr en el orden aquí escrito los siguientes querys, en su manejador de BD preferido:
	- insert into t_alumnos values (default,"John","Dow","Down",1);
	- insert into t_materias values (default,'matematicas',1);
	- insert into t_materias values (default,'programacion I',1);
	- insert into t_materias values (default,'ingenieria de sofware',1);

- Regresar a la consola y correr el siguiente comando:
	**php bin/console server:run**
	
	*Nota le regresara algo como esto en la consola:
	http://127.0.0.1:8000
        Favor de copiar y pegar esa url en su navegador, agregándole “api/doc”
	Así quedaría la URL final: http://127.0.0.1:8000/api/doc 
	Y es ahi donde visualizaría la API


_CREACION DE USUARIOS( IMPORTANTE )_

La API tiene seguridad con JWT, por lo tanto para hacer peticiones a la API, se necesita agregar un Header en cada petición con un token de acceso.

Por lo tanto a continuación enlistare los pasos a seguir:

- Crear un usuario en el endpoint(este endpoint no tiene seguridad con la finalidad de crear usuarios libremente):
	/api/users/
- Hacer login con los datos del usuario que se creo, en el siguiente endpoint:
	/api/login
	*Nota: en el parámetro “gethash” ponerle “true”, todo esto para que te regrese un token de acceso.
- Ahora ya teniendo el Token, es cuestión de agregarlo en cada header de los endpoints de la API, para poder acceder a ellos. No olvide agregar la palabra “bearer”.
	Ejemplo:
	Authorization : bearer + “mi_token”
